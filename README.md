zotero-open-with
================

This is a plug in for [Zotero](http://www.zotero.org/).

It adds a *Open With ...* right-klick-menu item to open a attachment with a special application instead only the system default of the file type.

Well the plugin is actual quite rudimentary, the path of the special application is hard coded. So for most of you it should not be useful without changing the source code.

Requirements
------------

Only *Firefox* with *Zotero plugin* works, **not** *Zotero standalone*. But I actually don't know why the *standalone* does not work, maybe they removed some libraries.